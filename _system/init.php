<?php
set_time_limit(0);
date_default_timezone_set("Europe/Stockholm");
ini_set('memory_limit','1024M');
ini_set('pcre.backtrack_limit', '10000000');
define("ROOT_PATH", __DIR__."/../");
define("TMP_PATH", __DIR__."/_tmp/");

@mkdir(TMP_PATH);

$Prefs = new Prefs();

function getRandomKey($length=15) {
	$keyChars = '23456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	$key = "";

	for ($index = 1; $index <= $length; $index++) {
		$randomNumber = mt_rand(1,strlen($keyChars));
		$key .= substr($keyChars,$randomNumber-1,1);
	}
		
	return $key;
}

function logger($log, $logname="default") {
	if (defined("LOG_PATH")) {
		$log_path = LOG_PATH;
	} elseif (defined("CUSTOMER_PATH")) {
		$log_path = CUSTOMER_PATH;
	} else {
		console("NOTICE! No log path defined.", 1);
		return;
	}
	
	if (!is_dir($log_path)) {
		console("NOTICE! Creating path to log.", 1);
		if (!mkdir($log_path, 0777, true)){
			console("NOTICE! Failed to create log-path ({$log_path})", 2);
		}
	}

	if (is_dir($log_path)) {
		$custom_logpath = $log_path.$logname.".log";
		$log = strftime("%F %T")." - ".$log;
		file_put_contents($custom_logpath, $log."\n", FILE_APPEND);
	} else {
		console("NOTICE! No log path.", 1);
	}
}

function __autoload($class_name) {
	require_once(ROOT_PATH."_system/_classes/".strtolower($class_name)."_class.php");
}

function sleeper($seconds=10) {
	for ($i=1; $i<=$seconds; $i++) {
		sleep(1);
		echo $i." ";
	}
}

function console ($message, $indent=0) {
	for($i=0; $i<$indent; $i++) {
		echo "    ";
	}
	
	echo $message."\n";
}