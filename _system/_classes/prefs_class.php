<?php
class Prefs {
	public $in_path = null;
	public $done_path = null;
	public $migration_config_path = null;
	public $migration_config = null;
	public $oc_server = null;
	public $oc_username = null;
	public $oc_password = null;
	public $oc_batch = null;
	
	public function __construct() {
		require_once (ROOT_PATH."_prefs/_prefs.php");
		$this->read_in_path();
		$this->read_done_path();
		$this->migration_config_path = ROOT_PATH."_prefs/_migration_config.xml";
		$this->read_migration_config_path();
		$this->read_migration_config();
		$this->set_server_settings();
		echo "========================\n";
	}
	
	private function set_server_settings() {
		$oc_server = (string)$this->migration_config->OC->server;
		$oc_username = (string)$this->migration_config->OC->username;
		$oc_password = (string)$this->migration_config->OC->password;
		
		$Internet = new Internet();
		
		$everything_ok = false;
		
		while(!$everything_ok) {
			echo "OC server ({$oc_server}): ";
			$handle = fopen ("php://stdin","r");
			$line = trim(fgets($handle));
			if ($line != "") {
				$oc_server = $line;
			}
			
			echo "OC username ({$oc_username}): ";
			$handle = fopen ("php://stdin","r");
			$line = trim(fgets($handle));
			if ($line != "") {
				$oc_username = $line;
			}
			
			echo "OC password (".preg_replace("/./", "*", $oc_password)."): ";
			system('stty -echo');
			$line = trim(fgets(STDIN));
			system('stty echo');
			// add a new line since the users CR didn't echo
			echo "\n";
			if ($line != "") {
				$oc_password = $line;
			}
			
			$context = [
				'http' => [
					'method' => 'GET',
					'header'  => "Authorization: Basic " . base64_encode("{$oc_username}:{$oc_password}")
				]
			];

			$context = stream_context_create($context);

			$data = @file_get_contents("http://{$oc_server}:8080/opencontent/infoandstats/ping", false, $context, 0, 1);

			if ($data === false) {
				console("ERROR! Server offline or credentials not accepted.", 1);
			} else {
				$everything_ok = true;
			}
		}
		
		$this->oc_server = $oc_server;
		$this->oc_username = $oc_username;
		$this->oc_password = $oc_password;
		
		$oc_batch = null;
		
		while ($oc_batch != "true" AND $oc_batch != "false") {
			$oc_batch = (string)$this->migration_config->OC->batch;
			echo "OC batch index ({$oc_batch}): ";
			$handle = fopen ("php://stdin","r");
			$line = trim(fgets($handle));
			if ($line != "") {
				$oc_batch = $line;
			}
		}
		
		$this->oc_batch = $oc_batch; 
	}
	
	private function read_migration_config() {
		if (is_readable($this->migration_config_path)) {
			$this->migration_config = simplexml_load_file($this->migration_config_path);
		} else {
			console("ERROR! Could not read migration config ({$this->migration_config_path}).", 1);
			die();
		}
	}
	
	private function read_migration_config_path() {
		$this->set_migration_config_path();
		$this->migration_config_path = file_get_contents(ROOT_PATH."_prefs/migration_config_path.conf");
		
		while(!is_file($this->migration_config_path)) {
			console("ERROR! Migration config ({$this->migration_config_path}) not available.", 1);
			$this->set_migration_config_path();
		}
	}

	private function set_migration_config_path() {
		if (is_readable(ROOT_PATH."_prefs/migration_config_path.conf")) {
			$this->migration_config_path = trim(file_get_contents(ROOT_PATH."_prefs/migration_config_path.conf"));
		}
		
		echo "Path to Migration config ({$this->migration_config_path}): ";
		
		$handle = fopen ("php://stdin","r");
		$line = trim(fgets($handle));
		
		if ($line != "" OR !is_readable(ROOT_PATH."_prefs/migration_config_path.conf")) {
			if ($line != $this->migration_config_path) {
				file_put_contents(ROOT_PATH."_prefs/migration_config_path.conf", $line);
				$this->migration_config_path = $line;	
			}
		}
	}
		
	private function read_in_path() {
		$this->set_read_path();
		
		$this->in_path = file_get_contents(ROOT_PATH."_prefs/in_path.conf");
		
		while(!is_dir($this->in_path)) {
			console("ERROR! PDF directory ({$this->in_path}) not available.", 1);
			$this->set_read_path();
		}
	}

	private function set_read_path() {
		if (is_readable(ROOT_PATH."_prefs/in_path.conf")) {
			$this->in_path = trim(file_get_contents(ROOT_PATH."_prefs/in_path.conf"));
		}
		
		echo "Path to directory with pdf files ({$this->in_path}): ";
		
		$handle = fopen ("php://stdin","r");
		$line = trim(fgets($handle));
		
		if ($line != "" OR !is_readable(ROOT_PATH."_prefs/in_path.conf")) {
			if (substr($line, -1) != "/") {
				$line .= "/";
			}
			
			if ($line != $this->in_path) {
				file_put_contents(ROOT_PATH."_prefs/in_path.conf", $line);
				$this->in_path = $line;	
			}
		}
	}
	
	private function read_done_path() {
		$this->set_done_path();
		
		$this->done_path = file_get_contents(ROOT_PATH."_prefs/done_path.conf");
		
		while(!is_dir($this->done_path)) {
			console("ERROR! Done directory ({$this->done_path}) not available.", 1);
			$this->set_done_path();
		}
	}

	private function set_done_path() {
		if (is_readable(ROOT_PATH."_prefs/done_path.conf")) {
			$this->done_path = trim(file_get_contents(ROOT_PATH."_prefs/done_path.conf"));
		}
		
		echo "Path to \"done\" directory ({$this->done_path}): ";
		
		$handle = fopen ("php://stdin","r");
		$line = trim(fgets($handle));
		
		if ($line != "" OR !is_readable(ROOT_PATH."_prefs/done_path.conf")) {
			if (substr($line, -1) != "/") {
				$line .= "/";
			}
			
			if ($line != $this->done_path) {
				file_put_contents(ROOT_PATH."_prefs/done_path.conf", $line);
				$this->done_path = $line;
			}
		}
	}
}