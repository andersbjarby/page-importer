<?php
/******************************************************************************/
/* Copyright 1995-2016 Infomaker Scandinavia AB. All rights reserved          */
/*                                                                            */
/* Infomaker Scandinavia permits you to use and modify this file in           */
/* accordance with the terms of the license agreement accompanying it         */
/* If you have received it from a source other than Infomaker Scandinavia     */
/* then use or modification requires prior written permission from Infomaker  */
/*                                                                            */
/******************************************************************************/

class Internet {
	public function ping($host="8.8.8.8") {
		$result = exec(PING.' -c 1 -W 1 '.escapeshellarg($host));
		
		if (preg_match("/0 packets received/", $result)) {
			return false;
		} else {
			return true;
		}
	}
	
	public function wait_for_access($url, $wait_for_internet=true) {
		if ($wait_for_internet) {
			while (!$this->ping()) {
				echo "\nNOTICE! No internet access... sleeping for 10 seconds.\n";
				sleeper(10);
			}
		
			$tmp = parse_url($url);
			if (!isset($tmp["port"])) {
				$tmp["port"] = "80";
			}
		
			$base_url = $tmp["scheme"]."://".$tmp["host"].":".$tmp["port"];
			
			$n = 0;
			while (!$this->online($base_url)) {
				if (is_int($wait_for_internet) and $n == $wait_for_internet) {
					echo "\nWARNING! Host ".$base_url." seems to be offline... Aborting this object.\n";
					return false;
				}
				echo "\nNOTICE! Host ".$base_url." seems to be offline... sleeping for 10 seconds.\n";
				sleeper(10);
				$n++;
			}
		}
	}
	
	public function online($url) {
		if (@file_get_contents($url,false,NULL,0,1)) {
			return true;
		} else {
			return false;
		}
	}
}
