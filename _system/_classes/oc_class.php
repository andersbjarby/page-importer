<?php
class OC {
	public function process_page($filepath, $filename) {
		$package_path = $filepath."_package/";
		
		console ("Creating temporary package directory ({$package_path}).", 3);
		@mkdir ($package_path);
		
		console ("Copy file ($filepath) to package ({$package_path}{$filename})", 3);
		copy($filepath, $package_path.$filename);
		
		console ("Extracting text from Page", 3);
		$this->extract_text_from_page($package_path.$filename);
		
		console ("Creating preview and thumbnail", 3);
		$this->create_bitmaps_for_page($package_path.$filename);
		
		console ("Extracting metadata from filename", 3);
		$metadata = $this->get_metadata_from_filename($filename);
		
		console ("Creating metadatafile", 3);
		$this->create_metadata($filename, $metadata, $package_path);
		
		console ("Uploading package", 3);
		$result = $this->upload_package($filename, $package_path);
		
		console ("Deleting temporary package directory ({$package_path}).", 3);
		$Files = new Files();
		$Files->delTree($package_path);
		
		return $result;
	}
	
	private function upload_package($filename, $package_path) {
		global $Prefs;
		
		$metadata_mimetype = (string)$Prefs->migration_config->metadata_mimetype;
		$oc_server = (string)$Prefs->oc_server;
		$oc_batch = (string)$Prefs->oc_batch;
		$oc_username = (string)$Prefs->oc_username;
		$oc_password = (string)$Prefs->oc_password;
		$upload_when_filename_exist_in_oc = (string)$Prefs->migration_config->OC->upload_when_filename_exist_in_oc;
		
		$Internet = new Internet();
		$Internet->wait_for_access("http://{$oc_server}:8080");
		
		if ($upload_when_filename_exist_in_oc == "false") {
			console ("Verifying if file already exists in OC", 4);
			
			$data = false;
					
			while (!$data) {
				$context = [
					'http' => [
						'method' => 'GET',
						'header'  => "Authorization: Basic " . base64_encode("{$oc_username}:{$oc_password}")
					]
				];

				$context = stream_context_create($context);

				$data = @file_get_contents("http://xlibris.internal.oc.mitm.infomaker.io:8080/opencontent/search?q=".urlencode(pathinfo($filename, PATHINFO_FILENAME))."+OR+DocumentName:".urlencode(pathinfo($filename, PATHINFO_FILENAME))."*&start=0&limit=1&property=uuid", false, $context);
				
				if (!$data) {
					echo "WARNING: failed to get data from source. Is {$oc_server} Offline? Will sleep for 30 seconds and retry.\n";
					sleeper(30);
				}

				$json = @json_decode($data);
							
				if (!$json) {
					echo "WARNING: failed to parse json. Using {$oc_server}/opencontent/search?q=".urlencode(pathinfo($filename, PATHINFO_FILENAME))."+OR+DocumentName:".urlencode(pathinfo($filename, PATHINFO_FILENAME))."*&start=0&limit=1&property=uuid. Will sleep for 30 seconds and retry.\n";
					sleeper(30);
					$data = false;
				}
			}
				    	
			$total_hits = $json->hits->totalHits;
			
			//var_dump ($json);
		
			if ($total_hits > 0) {
				console ("File already exist in OC (uuid:{$json->hits->hits[0]->id})", 4);
				return true;
			}
		}	
		
		$curl_string = CURL." -m 500 -s -S http://{$oc_server}:8080/opencontent/objectupload -u {$oc_username}:{$oc_password}";
							
		if ($oc_batch == "true") {
			$curl_string .= " -F batch=true ";
		}
		
		$curl_string .= " -F file=".$this->quotedata($filename);
		$curl_string .= " -F file-mimetype=".$this->quotedata("application/pdf");
		$curl_string .= " -F ".$this->quotedata($filename)."=@".$this->quotedata($package_path.$filename);
		
			
		$curl_string .= " -F metadata=".$this->quotedata("page.xml");
		$curl_string .= " -F metadata-mimetype=".$this->quotedata($metadata_mimetype);
		$curl_string .= " -F ".$this->quotedata("page.xml")."=@".$this->quotedata($package_path."page.xml");
		
		$curl_string .= " -F metadata2=".$this->quotedata("freetext.txt");
		$curl_string .= " -F metadata2-mimetype=".$this->quotedata("text/plain");
		$curl_string .= " -F ".$this->quotedata("freetext.txt")."=@".$this->quotedata($package_path."freetext.txt");
	
		$curl_string .= " -F thumb=".$this->quotedata("thumb.jpg");
		$curl_string .= " -F thumb-mimetype=".$this->quotedata("image/jpeg");
		$curl_string .= " -F ".$this->quotedata("thumb.jpg")."=@".$this->quotedata($package_path."thumb.jpg");
		
		$curl_string .= " -F preview=".$this->quotedata("preview.jpg");
		$curl_string .= " -F preview-mimetype=".$this->quotedata("image/jpeg");
		$curl_string .= " -F ".$this->quotedata("preview.jpg")."=@".$this->quotedata($package_path."preview.jpg");
		
		//echo $curl_string;
		
		$uploadresult = shell_exec($curl_string);
		
		console ("RESPONSE: {$uploadresult}", 4);
		
		logger("({$filename}) RESPONSE: {$uploadresult}.", "upload ({$oc_server})");

		if (preg_match("/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}/", $uploadresult)) {
			// Upload OK
			return true;
		}
		
		return false;
	}
	
	private function create_metadata($filename, $metadata, $package_path) {
		global $Prefs;
		
		$template_file_path = (string)$Prefs->migration_config->metadata_template;
		
		if (is_readable(ROOT_PATH."_prefs/{$template_file_path}")) {
			$template_file_path = ROOT_PATH."_prefs/{$template_file_path}";
		} elseif (is_readable($template_file_path)) {
			//
		} else {
			console ("ERROR! Coud not read template file path ({$template_file_path}).", 3);
			die();
		}
		$template = file_get_contents($template_file_path);
		
		$organization = (string)$Prefs->migration_config->organization;
		
		$id_interval = explode("-", (string)$Prefs->migration_config->random_id_interval);
		
		$id = mt_rand($id_interval[0], $id_interval[1]);
		
		/*
			%%organization%%
			%%id%%
			%%section_short%%
			%%section_long%%
			%%year%%
			%%month%%
			%%day%%
			%%filename%%
			%%page%%
			%%edition%%
			%%part%%
			%%product_short%%
			%%product_long%%
			%%subproduct_short%%
			%%subproduct_long%%
		*/
		
		$template = str_replace("%%organization%%", $organization, $template);
		$template = str_replace("%%id%%", $id, $template);
		$template = str_replace("%%section_short%%", $metadata["section_short"], $template);
		$template = str_replace("%%section_long%%", $metadata["section_long"], $template);
		$template = str_replace("%%year%%", $metadata["year"], $template);
		$template = str_replace("%%month%%", $metadata["month"], $template);
		$template = str_replace("%%day%%", $metadata["day"], $template);
		$template = str_replace("%%filename%%", $filename, $template);
		$template = str_replace("%%page%%", $metadata["page"], $template);
		$template = str_replace("%%edition%%", $metadata["edition"], $template);
		$template = str_replace("%%part%%", $metadata["part"], $template);
		$template = str_replace("%%product_short%%", $metadata["product_short"], $template);
		$template = str_replace("%%product_long%%", $metadata["product_long"], $template);
		$template = str_replace("%%subproduct_short%%", $metadata["subproduct_short"], $template);
		$template = str_replace("%%subproduct_long%%", $metadata["subproduct_long"], $template);
		
		file_put_contents("{$package_path}page.xml", $template);
	}
	
	private function get_metadata_from_filename($filename) {
		global $Prefs;
		$metadata = array();
		$pattern = (string)$Prefs->migration_config->pdf_name_pattern;
		
		$metadata["product"] = false;
		if (preg_match("/([P]{1,})/", $pattern, $matches)) {
			$metadata["product"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
		
		$metadata["subproduct"] = false;
		if (preg_match("/([B]{1,})/", $pattern, $matches)) {
			$metadata["subproduct"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
		
		$metadata["section"] = false;
		if (preg_match("/([S]{1,})/", $pattern, $matches)) {
			$metadata["section"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
		
		$metadata["year"] = "2000";
		if (preg_match("/([Y]{1,})/", $pattern, $matches)) {
			$metadata["year"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
			if (strlen($metadata["year"]) == 2) {
				$metadata["year"] = "20{$metadata["year"]}";
			}
		}
		
		$metadata["month"] = "01";
		if (preg_match("/([M]{1,})/", $pattern, $matches)) {
			$metadata["month"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
		
		$metadata["day"] = "01";
		if (preg_match("/([D]{1,})/", $pattern, $matches)) {
			$metadata["day"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
		
		$metadata["page"] = "001";
		if (preg_match("/([n]{1,})/", $pattern, $matches)) {
			$metadata["page"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
		
		$metadata["part"] = "A";
		if (preg_match("/([p]{1,})/", $pattern, $matches)) {
			$metadata["part"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
		
		$metadata["edition"] = "A";
		if (preg_match("/([E]{1,})/", $pattern, $matches)) {
			$metadata["edition"] = substr($filename, strpos($pattern,$matches[1]), strlen($matches[1]));
		}
			
		$metadata["product_short"] = "";
		$metadata["product_long"] = "";
		
		if ($Prefs->migration_config->xpath("mappings/product/default")) {
			$metadata["product_long"] = (string)$Prefs->migration_config->xpath("mappings/product/default/@long")[0];
			$metadata["product_short"] = (string)$Prefs->migration_config->xpath("mappings/product/default/@short")[0];
		}
		
		if ($metadata["product"]) {
			if($Prefs->migration_config->xpath("mappings/product/option[@match='{$metadata["product"]}']")) {
				$metadata["product_long"] = (string)$Prefs->migration_config->xpath("mappings/product/option[@match='{$metadata["product"]}']/@long")[0];
				$metadata["product_short"] = (string)$Prefs->migration_config->xpath("mappings/product/option[@match='{$metadata["product"]}']/@short")[0];
			}
		}
		
		$metadata["subproduct_short"] = "";
		$metadata["subproduct_long"] = "";
		
		if ($Prefs->migration_config->xpath("mappings/subproduct/default")) {
			$metadata["subproduct_long"] = (string)$Prefs->migration_config->xpath("mappings/subproduct/default/@long")[0];
			$metadata["subproduct_short"] = (string)$Prefs->migration_config->xpath("mappings/subproduct/default/@short")[0];
		}
		
		if ($metadata["subproduct"]) {
			if($Prefs->migration_config->xpath("mappings/subproduct/option[@match='{$metadata["subproduct"]}']")) {
				$metadata["subproduct_long"] = (string)$Prefs->migration_config->xpath("mappings/subproduct/option[@match='{$metadata["subproduct"]}']/@long")[0];
				$metadata["subproduct_short"] = (string)$Prefs->migration_config->xpath("mappings/subproduct/option[@match='{$metadata["subproduct"]}']/@short")[0];
			}
		}
		
		$metadata["section_short"] = "";
		$metadata["section_long"] = "";
		
		if ($Prefs->migration_config->xpath("mappings/section/default")) {
			$metadata["section_long"] = (string)$Prefs->migration_config->xpath("mappings/section/default/@long")[0];
			$metadata["section_short"] = (string)$Prefs->migration_config->xpath("mappings/section/default/@short")[0];
		}
		
		if ($metadata["section"]) {
			if($Prefs->migration_config->xpath("mappings/section/option[@match='{$section}']")) {
				$metadata["section_long"] = (string)$Prefs->migration_config->xpath("mappings/section/option[@match='{$metadata["section"]}']/@long")[0];
				$metadata["section_short"] = (string)$Prefs->migration_config->xpath("mappings/section/option[@match='{$metadata["section"]}']/@short")[0];
			} 
		}
		
		console ("Pubdate: {$metadata["year"]}-{$metadata["month"]}-{$metadata["day"]}", 4);
		console ("Page: {$metadata["page"]}", 4);
		console ("Section: {$metadata["section_long"]}", 4);
		console ("Product: {$metadata["product_long"]} ({$metadata["product_short"]})", 4);
		
		return $metadata;
	}
	
	private function create_bitmaps_for_page($filepath) {
		if (PDF_TO_BITMAP_GENERATOR == "gs") {
			$tmp_bitmap = "{$filepath}.tif";
			exec (GS." -dNOGC -dBATCH -dNOPAUSE -dMaxBitmap=50000000 -dTextAlphaBits=4 -sDEVICE=tiff24nc -dGraphicsAlphaBits=4 -r144x144 -dLastPage=1 -dNumRenderingThreads=12 -dBandBufferSpace=500000000 -sBandListStorage=memory -dBufferSpace=1000000000 -sOutputFile=\"{$tmp_bitmap}\" \"{$filepath}\"");
		} else {
			$tmp_bitmap = "{$filepath}.png";
			exec (MUPDF." -F png -w ".PREVIEW_MAX_SIZE." -h ".PREVIEW_MAX_SIZE." -o \"{$tmp_bitmap}\" \"{$filepath}\"");
		}

		$this->create_bitmap(PREVIEW_MAX_SIZE, $tmp_bitmap, dirname($filepath)."/preview.jpg");
		$this->create_bitmap(THUMB_MAX_SIZE, $tmp_bitmap, dirname($filepath)."/thumb.jpg");
		
		unlink($tmp_bitmap);

	}
	
	private function extract_text_from_page($filepath) {
		$destination = dirname($filepath)."/freetext.txt";
		exec(PDF_TO_TEXT." -enc UTF-8 \"{$filepath}\" \"{$destination}\" &>/dev/null");
		$text = file_get_contents($destination);
		$Filters = new Filters();
		$text = $Filters->easy_fix($text);
		file_put_contents($destination, $text);
	}
	
	private function create_bitmap($maxDimensions, $inPath, $outPath) {
		if (THUMB_GENERATOR == "sips") {
			exec(SIPS." --resampleHeightWidthMax {$maxDimensions} --setProperty format jpeg --setProperty formatOptions normal \"{$inPath}\" --out \"{$outPath}\"");
		} elseif (THUMB_GENERATOR == "im") {
			exec (IM_CONVERT." \"{$inPath}\" -background \#ffffff -flatten -resize {$maxDimensions}x{$maxDimensions}\\> \"{$outPath}\"");
		} else {
			exec (GM_CONVERT." \"{$inPath}\" -background \#ffffff -flatten -resize {$maxDimensions}x{$maxDimensions}\\> \"{$outPath}\"");
		}

		if (!is_file($outPath)) {
			// SIPS fallback
			exec(SIPS." --resampleHeightWidthMax {$maxDimensions} --setProperty format jpeg --setProperty formatOptions normal \"{$inPath}\" --out \"{$outPath}\"");
		}
	}
	
	private function quotedata($data) {
		$data = preg_replace("/\"/", "\\\"", $data);
		return "\"".$data."\"";
	}
}