<?php
class Filters {
	public function strip_low_ascii($data, $linebreaks=false) {
		$with = "";
		$data = str_replace("\0", $with, $data);
		$data = preg_replace("/\x01/", $with, $data);
		$data = preg_replace("/\x02/", "[marker]", $data);
		$data = preg_replace("/\x03/", $with, $data);
		$data = preg_replace("/\x04/", $with, $data);
		$data = preg_replace("/\x05/", $with, $data);
		$data = preg_replace("/\x06/", $with, $data);
		$data = preg_replace("/\x07/", $with, $data);
		$data = preg_replace("/\x08/", $with, $data);
		$data = preg_replace("/\x09/", " ", $data); //tab
		$data = preg_replace("/\x0b/", $with, $data);
		$data = preg_replace("/\x0c/", $with, $data);
		$data = preg_replace("/\x0e/", $with, $data);
		$data = preg_replace("/\x0f/", $with, $data);
		$data = preg_replace("/\x10/", $with, $data);
		$data = preg_replace("/\x11/", $with, $data);
		$data = preg_replace("/\x12/", $with, $data);
		$data = preg_replace("/\x13/", $with, $data);
		$data = preg_replace("/\x14/", $with, $data);
		$data = preg_replace("/\x15/", $with, $data);
		$data = preg_replace("/\x16/", $with, $data);
		$data = preg_replace("/\x17/", $with, $data);
		$data = preg_replace("/\x18/", $with, $data);
		$data = preg_replace("/\x19/", $with, $data);
		$data = preg_replace("/\x1a/", $with, $data);
		$data = preg_replace("/\x1b/", $with, $data);
		$data = preg_replace("/\x1c/", $with, $data);
		$data = preg_replace("/\x1d/", $with, $data);
		$data = preg_replace("/\x1e/", $with, $data);
		$data = preg_replace("/\x1f/", $with, $data);

		if($linebreaks) {
			$data = preg_replace("/\x0a/", " ", $data);
			$data = preg_replace("/\x0d/", " ", $data);
		}

		return $data;
	}
	
	public function sanitize_xml_value($data) {
		$data = preg_replace("/\&/", "&amp;", $data);
		$data = preg_replace("/\</", "&lt;", $data);
		$data = preg_replace("/\>/", "&gt;", $data);
		return $data;
	}
	
	public function easy_fix($data, $linebreaks=false) {
		return trim($this->sanitize_xml_value($this->strip_low_ascii($data, $linebreaks)));
	}
	
	public function format_xml ($xml_string) {
		$doc = new DOMDocument('1.0');
		$doc->preserveWhiteSpace = false;
		$doc->loadXML($xml_string);
		$doc->formatOutput = true;
		$doc->substituteEntities = true;
		return $doc->saveXML();
	}
	
	public function wash_utf8($string) {
		return iconv('UTF-8', 'UTF-8//IGNORE', $string);
	} 
}