<?php
	class Files {
		public function iterate($path, $done_path, $callback_function) {
			if (!is_dir($path)) {
				console("ERROR! The folder (".$path.") is missing.");
				return false;
			}
			
			echo "Iterating folder (".$path.")\n";
			
			if ($DIR = opendir($path)) {
				$dir_array = array();

				while (false !== ($tmp = readdir($DIR))) {
					if ($tmp && $tmp != ".DS_Store") {
						$dir_array[] = $tmp;
					}
				}
						
				//slumpa ordningen i arrayen för att effektiveras genom att minska krockar och väntan
				shuffle($dir_array);
				
				foreach ($dir_array as $filename) {
					if ($filename != "." AND $filename != "..") {
						$filepath = $path.$filename;
						
						if (is_file($filepath)) {
							console("PROCESSING $filename.", 1);
							
							$process_file = true;
							
							$original_filepath = $filepath;
							
							$tmp_path = TMP_PATH.$filename;
							
							if (is_readable($tmp_path)) {
								console("NOTICE! Detected identical tmp-jobb ({$tmp_path}), waiting 5 secs before deleting.", 1);
								sleeper(5);
								unlink($tmp_path);
							}

							if (is_readable($filepath)) {
								if(rename($filepath, $tmp_path)) {
									console("Moving from {$filepath} to {$tmp_path}.", 2);
									$filepath = $tmp_path;
								} else {
									console("NOTICE! File ({$filepath}) already processed.", 1);
									$process_file = false;
								}
							} else {
								console("NOTICE! File ({$filepath}) already processed.", 1);
								$process_file = false;
							}
							
							if ($process_file) {
								$result = $callback_function($filepath, $filename, $original_filepath);
								
								if ($result) {
									$tmp_done_path = $done_path.$filename;
									console("File ({$filepath}) processed.", 2);
									console("Moving from {$tmp_path} to {$tmp_done_path}.", 2);
									rename ($filepath, $tmp_done_path);	
								} else {
									@mkdir ($done_path."_fail/");
									$tmp_done_path = $done_path."_fail/".$filename;
									console("File ({$filepath}) failed.", 2);
									console("Moving from {$tmp_path} to {$tmp_done_path}.", 2);
									rename ($filepath, $tmp_done_path);
								}
								console("---------------------------------", 1);
							}
						} elseif (is_dir($filepath)) {
							$this->iterate($filepath."/", $done_path, $callback_function);
						}
					}
				}
			} else {
				console("ERROR! The folder (".$path.") is unreadable.");
				return false;
			}
		}
		
		public function delTree($dir) { 
			$files = array_diff(scandir($dir), array('.','..')); 
				
			foreach ($files as $file) { 
				if(is_dir("$dir/$file")) {
					$this->delTree("$dir/$file");
				} else {
					//echo "deleting file : {$dir}/{$file}\n";
					unlink("$dir/$file");
				}
			} 
			//echo "deleting directory: {$dir}\n";
			rmdir($dir);
		}
	}
?>