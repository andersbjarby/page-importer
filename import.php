<?php
require_once ("_system/init.php");	

define("LOG_PATH", __DIR__."/_log/");

$Files = new Files();
$Files->iterate($Prefs->in_path, $Prefs->done_path, "process_file");

function process_file($filepath, $filename, $original_filepath) {
	$OC = new OC();
	return $OC->process_page($filepath, $filename);
}

