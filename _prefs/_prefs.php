<?php
/*
PDF_TO_TEXT
============
pdftotext is a tool that comes with the Poppler package, used when extracting text from PDF docuents.
http://poppler.freedesktop.org
*/
define("PDF_TO_TEXT", "/opt/local/bin/pdftotext");

/*
CURL
=====
Path to curl binary
*/
define("CURL", "/usr/bin/curl");

/*
CURL
=====
Path to ping binary
*/
define("PING", "/sbin/ping");

/*
PDF_TO_BITMAP_GENERATOR
========================
Defines which pdf to bitmap generator that should be used. Ghostscript(gs) or MuDraw(mu).
*/
define("PDF_TO_BITMAP_GENERATOR", "mu");

/*
GS
===
path to Ghostscript binary.
https://www.ghostscript.com/
*/
define("GS", "/usr/local/bin/gs");

/*
MUPDF
======
path to MuDraw binary.
https://mupdf.com/
*/
define("MUPDF", "/usr/local/bin/mudraw");

/*
THUMB_GENERATOR
================
Defines which engine that should be used for generating thumbnails and previews. 
Sips, Imagemagick(im) or Graphicsmagick(gm).
https://www.imagemagick.org/
http://www.graphicsmagick.org
*/
define("THUMB_GENERATOR", "gm");

/*
SIPS
=====
Path to Sips binary. Native macOS.
*/
define("SIPS", "/usr/bin/sips");

/*
IM_CONVERT
===========
Path to Imagemagick convert binary.
https://www.imagemagick.org
*/
define("IM_CONVERT", "/usr/local/bin/convert");

/*
GM_CONVERT
===========
Path to Graphicsmagick convert binary.
http://www.graphicsmagick.org
*/
define("GM_CONVERT", "/usr/local/bin/gm convert");

/*
PREVIEW_MAX_SIZE, THUMB_MAX_SIZE
=================================
Max dimensions for thumbnails and previews.
*/
define("PREVIEW_MAX_SIZE", 900);
define("THUMB_MAX_SIZE", 160);

